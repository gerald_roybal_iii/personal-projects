// Uses malloc to generate a random string
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <time.h>

using namespace std;

int main()
{
    int i, n;
    char * buffer;
    srand (time(NULL));

    cout << "How long do you want the string? ";
    cin >> i;

    buffer = (char*) malloc (i);
    if (buffer == NULL)
        return 1;

    for(n = 0; n<i; n++)
        buffer[n] = rand()%26+'a';
    buffer[i] = '\0';

    cout << "Random string: " << buffer << " \n";
    free(buffer);

    return 0;
}
