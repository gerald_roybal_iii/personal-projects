/*
Pancake Glutton
Requires:
 variables, data types, and numerical operators
 basic input/output
 logic (if statements, switch statements)
 loops (for, while, do-while)
 arrays

 VERSION 1 - Write a program that asks the user to enter the number of pancakes eaten for breakfast by 10 different people (Person 1, Person 2, ..., Person 10)
 Once the data has been entered the program must analyze the data and output which person ate the most pancakes for breakfast.

 VERSION 2 -Modify the program so that it also outputs which person ate the least number of pancakes for breakfast.

 VERSION 3 -Modify the program so that it outputs a list in order of number of pancakes eaten of all 10 people.
 i.e.
 Person 4: ate 10 pancakes
 Person 3: ate 7 pancakes
 Person 8: ate 4 pancakes
 ...
 Person 5: ate 0 pancakes
 */

#include <iostream>
using namespace std;

int main()
{
    int numElements = 10;
    int people [numElements];
    int  numPancakes = 0;
    int lowestSoFar = 0;
    int lowestPerson = -1;
    int sortQuantity;
    int i;
    bool selectionSorted = false;

    //input
    for(i = 0; i < 10; i++)
    {
        cout << "\nPlease enter how many pancakes were eaten by person " << i+1 << ": ";
        cin >> numPancakes;
        people[i] = numPancakes;
    }

    //Insertion Sort
    int temp;
    for(i = 1; i < 10; i++)
    {
        int j = i - 1;
        temp = people[i];

        while(j >= 0 && people[j] > temp)
        {
            people[j+1] = people[j];
            j = j-1;
        }

        people[j+1] = temp;
    }

    // output
    for(i = 0; i < 10; i++)
    {
        cout << "Person " << i+1<< " ate " << people[i] << " pancakes\n";
    }

    return 0;
}
