//Implementation of binary search
#include <iostream>

using namespace std;

int main()
{
    int playerGuess;
    int high, low, mid;
    string correct, highLow;

    high = 10000;
    mid = 5000;
    low = 0;
    correct = "n";
    cout << "Think of a number between 1 and 10000. I will try to guess that number." << endl;

    while(correct != "y")
    {
        cout << "\nIs your number " << mid << " ?\ny or n: ";
        cin >> correct;
        if(correct == "y")
            break; //Answer found

        cout << "\nIs your number higher (h) or lower (l)?\nh or l:";
        cin >> highLow;
        if(highLow == "h")
        {
            low = mid;
            mid = (low + high) / 2;
        }
        else
        {
            high = mid;
            mid = (low + high) / 2;
        }
    }

        cout << "\n\nI win.\n\n";
        return 0;
    }
