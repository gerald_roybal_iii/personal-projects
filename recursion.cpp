//practicing recursion
#include <iostream>
using namespace std;

int countZeros(int amount)
{
    int *coins = new int[amount+1];
    coins[0] = 1;

    for(int i = 1; i <= amount; ++i) {
        //debug
        //cout << "i = " << i << " - coins[i/2] = " << coins[i/2] << " coins[i/3] = " << coins[i/3] << " coins[i/4] = " << coins[i/4] << endl;
        coins[i] = coins[i/2] + coins[i/3] + coins[i/4];
    }
    int c = coins[amount];
    delete[](coins);
    return c;
}

int main(int argc, char **argv)
{
    int total, input;
    cout << "A machine intakes a coin and outputs 3 coins of different worths: \nInput Value / 2, Input Value / 3, and Input Value / 4, all rounded down.\nEventually it's all zeros, but let's find out how many zeros \nan arbitrary input will yield.\n\nInput the value of your coin, please: ";
    cin >> input;
    total = countZeros(input);

    cout << total << endl;
}
