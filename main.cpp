/*Grading Program
Requires:
 variables, data types, and numerical operators
 basic input/output
 logic (if statements, switch statements)

 Write a program that allows the user to enter the grade scored in a programming class (0-100).
 If the user scored a 100 then notify the user that they got a perfect score.

 Modify the program so that if the user scored a 90-100 it informs the user that they scored an A

 Modify the program so that it will notify the user of their letter grade
 0-59 F 60-69 D 70-79 C 80-89 B 90-100 A
*/

#include <iostream>

using namespace std;

void perfectScore(int score)
{
    if (score == 100)
        cout << "PERFECT SCORE!\n\n"<<endl;
}

int main()
{
    int userScore = 0;
    bool quit = false;

    while(quit == false)
    {
        cout << "Please enter your score (or -1 to quit): ";
        cin >> userScore;

        if(userScore < 0)
            quit = true;
        else if(userScore < 60)
            cout << "\nYou got an F" << endl;
        else if(userScore < 70)
            cout << "\nYou got a D" << endl;
        else if(userScore < 80)
            cout << "\nYou got a C" << endl;
        else if(userScore < 90)
            cout << "\nYou got a B" << endl;
        else
        {
            cout << "\nYou got an A" << endl;
            perfectScore(userScore);
        }
    }

    return 0;

}
