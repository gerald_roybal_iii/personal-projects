//Calculate minimum change needed to equal a value
#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    float input, remaining;
    int quarters, dimes, nickles, pennies;
    string finalAmount = "You will receive the following change: ";
    quarters = 0;
    dimes = 0;
    nickles = 0;
    pennies = 0;

    cout << "Input a price and we will tell you \nthe minimum number of coins needed give change: ";
    cin >> input;

    input = int((input) * 100) + 1; // not sure why it's taking 1 away
    //cout << "input = " << input << " q - " << quarters << " d - " << dimes << " n - " << nickles << " p - " << pennies << endl;

    quarters = input / 25;
    //cout << "input = " << input << " q - " << quarters << " d - " << dimes << " n - " << nickles << " p - " << pennies << endl;

    input -= quarters * 25;
    dimes = input / 10;
    //cout << "input = " << input << " q - " << quarters << " d - " << dimes << " n - " << nickles << " p - " << pennies << endl;

    input -= dimes * 10;
    nickles = input / 5;
    //cout << "input = " << input << " q - " << quarters << " d - " << dimes << " n - " << nickles << " p - " << pennies << endl;

    input -= nickles * 5;
    pennies = input;
    //cout << "input = " << input << " q - " << quarters << " d - " << dimes << " n - " << nickles << " p - " << pennies << endl;

    cout << finalAmount;
    if(quarters != 0)
        cout << "\n" << quarters << " Quarters";
    if(dimes != 0)
        cout << "\n" << dimes << " Dimes";
    if(nickles != 0)
        cout << "\n" << nickles << " Nickels";
    if(pennies != 0)
        cout << "\n" << pennies << " Pennies";
    return 0;
}
