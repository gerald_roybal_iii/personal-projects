#include <iostream>
#include <vector>

using namespace std;

class stacks
{
private:
    vector<int> obj;

public:
    void push(int);
    int pop();
    int stackSize();
};

void stacks::push(int no)
{
    obj.push_back(no);
}

int stacks::pop()
{
   int popped;

   if( obj.size() > 0)
    {
        popped = obj.back();
        obj.pop_back();
        return popped;
    }
    else
    {
        cout << "The stack is empty, comrade!" << endl;
    }
}

int stacks::stackSize()
{
    return obj.size();
}


int main()
{
    stacks* stackYes = new stacks();
    stackYes->push(10);
    stackYes->push(20);

    for(int i = 0; i < 1000; i++)
    {
        stackYes->push(i);
    }

    cout << stackYes->stackSize() << endl;

    stackYes->pop();
    cout << stackYes->stackSize() << endl;

    for(int i = 0; i < 1002; i++)
    {
        stackYes->pop();
        cout << stackYes->stackSize() << endl;
    }

}

