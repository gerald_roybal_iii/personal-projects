/*
Cola Machine
Requires:
 variables, data types, and numerical operators
 basic input/output
 logic (if statements, switch statements)

 Write a program that presents the user w/ a choice of your 5 favorite beverages (Coke, Water, Sprite, ... , Whatever).
 Then allow the user to choose a beverage by entering a number 1-5.
 Output which beverage they chose.

 If you program uses if statements instead of a switch statement, modify it to use a switch statement.
 If instead your program uses a switch statement, modify it to use if/else-if statements.

 Modify the program so that if the user enters a choice other than 1-5 then it will output "Error. choice was not valid, here is your money back."


#include <iostream>
using namespace std;

int main()
{
    int choice;
    bool quit = false;

    while(quit == false)
    {
        cout << "\nSoda Menu:\n1\tCoke\n2\tPepsi\n3\tDr. Pepper\n4\tMountain Dew\n5\tCC Lemon\n\nPlease enter the number of your selection: ";
        cin >> choice;

        switch(choice)
        {
        case 1:
            cout << "\nCoca-Cola brings happiness.\n";
            break;
        case 2:
            cout << "\nPepsi, for those who think young.\n";
            break;
        case 3:
            cout << "\nWouldn't you like to be a Pepper?\n";
            break;
        case 4:
            cout << "\nMountain Dew. It's pretty good.\n";
            break;
        case 5:
            cout << "\nCC Lemon is the god of all soda. Please worship desu.\n";
            break;
        case -1:
            quit = true;
            break;
        default:
            cout << "\nError. choice was not valid, here is your money back.\n";
        }
    }

    return 0;
}
*/
